package com.example.myapplication

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.TextView
import com.example.myapplication4.R


class MainActivity : AppCompatActivity() {
    private lateinit var resultTextView: TextView
    private var operand = 0.0
    private var operation = ""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        resultTextView = findViewById(R.id.resultTextView)
    }

    fun numberClick(clickedView: View) {
        if (clickedView is TextView) {
            var result = resultTextView.text.toString()
            var number = clickedView.text.toString()

            if (result == "0") {
                result = ""
            }
            resultTextView.text = result + number

        }
    }

    fun operationClick(clickedView: View) {
        if (clickedView is TextView) {
            operand = resultTextView.text.toString().toDouble()
            operation = clickedView.text.toString()

            resultTextView.text = ""

        }
    }


    fun equalClick(clickedView: View) {
        val secOperand = resultTextView.text.toString().toDouble()
        when (operation) {
            "+" -> resultTextView.text = (operand + secOperand).toString()
            "-" -> resultTextView.text = (operand - secOperand).toString()
            "*" -> resultTextView.text = (operand * secOperand).toString()
            "/" -> resultTextView.text = (operand / secOperand).toString()
        }
    }
    fun ClearBTN (clickedView: View){
        resultTextView.text = "";
    }
    fun DelBTN (clickedView: View){
        var s: String = resultTextView.text.toString()
        s = s.substring(0, s.length - 1)
        resultTextView.text = s
    }
}